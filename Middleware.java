package com.example.demo.repository;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl;

import javax.persistence.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import java.lang.reflect.Field;

//public class Middleware {
//
//    public static String generateCreateTableScript(Class<?> concreteDecoratorClass, String tableName, List<String> deletedField) {
//
//        List<Field> allFields = new ArrayList<>();
//        Class<?> clazz = concreteDecoratorClass;
//
//        while (clazz != null) {
//            for (Field field : getFields(clazz)) {
//                if (!deletedField.contains(field.getName())) {
//                    allFields.add(field);
//                }
//            }
//            clazz = clazz.getSuperclass();
//        }
//
//        StringBuilder sb = new StringBuilder();
//        sb.append("CREATE TABLE ").append(tableName).append(" (\n");
//        for (int i = 0; i < allFields.size(); i++) {
//            Field field = allFields.get(i);
//            sb.append("  ").append(field.getName()).append(" ").append(getDataType(field));
//            if (i < allFields.size() - 1) {
//                sb.append(",");
//            }
//            sb.append("\n");
//        }
//        sb.append(");");
//
//        RepositoryUtil<?> repositoryUtil = new RepositoryUtil<>(concreteDecoratorClass);
//
//        repositoryUtil.createTable(sb.toString());
//
//        return sb.toString();
//    }
//
//    public static List<?> getAllObjects(String tableName, Class<?> concreteDecoratorClass) {
//        RepositoryUtil<?> repositoryUtil = new RepositoryUtil<>(concreteDecoratorClass);
//
//        List<?> result = repositoryUtil.getAllObject(tableName);
//
//        return result;
//    }
//
//    public static void createObject(Object object, Class<?> concreteDecoratorClass) {
//        RepositoryUtil<Object> repositoryUtil = new RepositoryUtil<>(concreteDecoratorClass);
//
//        repositoryUtil.saveObject(object);
//    }
//
//    private static List<Field> getFields(Class<?> clazz) {
//        List<Field> fields = new ArrayList<>();
//        for (Field field : clazz.getDeclaredFields()) {
//            // Exclude static fields
//            if (!java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
//                fields.add(field);
//            }
//        }
//        return fields;
//    }
//
//    private static String getDataType(Field field) {
//        Class<?> type = field.getType();
//        if (type == String.class) {
//            return "VARCHAR(255)";
//        } else if (type == int.class || type == Integer.class) {
//            return "INT";
//        } else if (type == long.class || type == Long.class) {
//            return "BIGINT";
//        } else if (type == float.class || type == Float.class) {
//            return "FLOAT";
//        } else if (type == double.class || type == Double.class) {
//            return "DOUBLE";
//        } else if (type == boolean.class || type == Boolean.class) {
//            return "BOOLEAN";
//        } else {
//            // Default to VARCHAR(255) for unknown types
//            return "VARCHAR(255)";
//        }
//    }
//}

public class Middleware extends ImplicitNamingStrategyLegacyJpaImpl {

    @Override
    public Identifier determineForeignKeyName(ImplicitForeignKeyNameSource source) {
        // Exclude the specific foreign key constraint by returning null
        if (source.getColumnNames().contains("record_idprogram")) {
            return null;
        }

        // Delegate to the default naming strategy for other cases
        return super.determineForeignKeyName(source);
    }

    public static void insertData(Class<?> clazz, Map<String, Object> dataMap) {
        StringBuilder sqlInsert = new StringBuilder();
        List<Class<?>> classes = new ArrayList<>();
        List<String> fieldList = new ArrayList<>();
        Field identifierField = null;
        Map<String, String> tableNames = new HashMap<>();
        Map<String, List<String>> fields = new HashMap<>();
        Map<String, String> fieldDescriptions = new HashMap<>();
        List<Class<?>> abstractDecoratorClasses = new ArrayList<>();
        int totalField = 0;
        int totalClasses = 0;
        String[] packageName = Arrays.copyOf(Middleware.class.getPackageName().split("\\."), 3);

        while (!clazz.getName().equals("java.lang.Object")) {
            if (clazz.getAnnotation(Table.class) != null) {
                tableNames.put(clazz.getName(), clazz.getAnnotation(Table.class).name());
            }

            if (clazz.getAnnotation(Entity.class) != null) {
                List<String> fieldNames = new ArrayList<>();
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Id.class)) {
                        identifierField = field;
                    }

                    if (!isPrimitiveOrWrapper(field.getType())) {
                        if (field.getAnnotation(ManyToOne.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToOne.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(OneToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(OneToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(ManyToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        }
                    } else {
                        fieldNames.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldList.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT), field.getType().getSimpleName());
                        totalField++;
                    }
                }
                classes.add(clazz);
                fields.put(clazz.getName(), fieldNames);
                totalClasses++;
            } else {
                abstractDecoratorClasses.add(clazz);
            }
            clazz = clazz.getSuperclass();
        }

        RepositoryUtil<?> repositoryUtil = new RepositoryUtil<>(clazz);
        for (int i = classes.size() - 1; i >= 0; i--) {
            Class<?> cls = classes.get(i);
            StringBuilder sql = new StringBuilder(String.format("INSERT INTO %s (", tableNames.get(cls.getName())));

            List<String> fieldOfEntity = fields.get(cls.getName());
            if (fieldOfEntity.size() == 0 || !fieldOfEntity.contains(identifierField.getName().toLowerCase(Locale.ROOT))) {
                fieldOfEntity.add(identifierField.getName().toLowerCase(Locale.ROOT));
                fields.put(cls.getName(), fieldOfEntity);
            }

            int idx = 1;
            for (String fieldName : fields.get(cls.getName())) {
                sql.append(fieldName);
                if (idx != fields.get(cls.getName()).size()) {
                    sql.append(", ");
                }
                idx++;
            }
            sql.append(") VALUES (");
            idx = 1;
            for (String fieldName : fields.get(cls.getName())) {
                sql.append("'").append(dataMap.get(fieldName)).append("'");
                if (idx != fields.get(cls.getName()).size()) {
                    sql.append(", ");
                }
                idx++;
            }
            sql.append(");\n");
            sqlInsert.append(sql);
        }
        repositoryUtil.executeNativeQuery(sqlInsert);
    }

    public static List<Map<String, Map<String, Object>>> getAllData(Class<?> clazz) {
        StringBuilder sql = new StringBuilder("SELECT ");
        StringBuilder tableSql = new StringBuilder("FROM ");
        List<Class<?>> classes = new ArrayList<>();
        List<String> fieldList = new ArrayList<>();
        Field identifierField = null;
        Map<String, String> tableNames = new HashMap<>();
        Map<String, List<String>> fields = new HashMap<>();
        Map<String, String> fieldDescriptions = new HashMap<>();
        List<Class<?>> abstractDecoratorClasses = new ArrayList<>();
        int totalField = 0;
        int totalClasses = 0;

        while (!clazz.getName().equals("java.lang.Object")) {
            if (clazz.getAnnotation(Table.class) != null) {
                tableNames.put(clazz.getName(), clazz.getAnnotation(Table.class).name());
            }

            if (clazz.getAnnotation(Entity.class) != null) {
                List<String> fieldNames = new ArrayList<>();
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Id.class)) {
                        identifierField = field;
                    }

                    if (!isPrimitiveOrWrapper(field.getType())) {
                        if (field.getAnnotation(ManyToOne.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToOne.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(OneToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(OneToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(ManyToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        }
                    } else {
                        fieldNames.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldList.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT), field.getType().getSimpleName());
                        totalField++;
                    }
                }
                classes.add(clazz);
                fields.put(clazz.getName(), fieldNames);
                totalClasses++;
            } else {
                abstractDecoratorClasses.add(clazz);
            }
            clazz = clazz.getSuperclass();
        }

        boolean firstTime = true;
        String tableNameBefore = "";
        int fieldProcessed = 0;
        int classProcessed = 0;
        for (Class<?> entityClass : classes) {
            String className = entityClass.getName();
            String tableName = tableNames.get(className);
            classProcessed++;

            for (String fieldName : fields.get(className)) {
                fieldProcessed++;
                if (fieldProcessed == totalField && classProcessed == totalClasses) {
                    // Last fields entry
                    sql.append(String.format("%s.%s \n", tableName, fieldName));

                } else {
                    sql.append(String.format("%s.%s, ", tableName, fieldName));
                }
            }

            if (firstTime) {
                tableSql.append(tableName).append("\n");
                firstTime = false;
                tableNameBefore = tableName;
            } else {
                tableSql.append("JOIN ").append(tableName).append("\n")
                        .append(" ON ")
                        .append(String.format("%s.%s", tableName, identifierField.getName()))
                        .append(" = ")
                        .append(String.format("%s.%s", tableNameBefore, identifierField.getName()))
                        .append("\n");
            }
        }
        sql.append(tableSql);

        RepositoryUtil<?> repositoryUtil = new RepositoryUtil<>(clazz);
        List<Object[]> res = repositoryUtil.getAllObject(sql, fieldList, fieldDescriptions);
        List<Map<String, Map<String, Object>>> resultMap = new ArrayList<>();

        for (Object[] result : res) {
            Map<String, Map<String, Object>> classMap = new HashMap<>();
            int idx = 0;
            for (Class<?> cls : classes) {
                Map<String, Object> fieldMap = new HashMap<>();

                for (Field field : cls.getDeclaredFields()) {
                    fieldMap.put(field.getName(), result[idx]);
                    idx++;
                }
                classMap.put(cls.getName(), fieldMap);
            }
            resultMap.add(classMap);
        }

        return resultMap;
    }

    public static Map<String, Map<String, Object>> getData(Class<?> clazz, UUID id) {
        StringBuilder sql = new StringBuilder("SELECT ");
        StringBuilder tableSql = new StringBuilder("FROM ");
        List<Class<?>> classes = new ArrayList<>();
        List<String> fieldList = new ArrayList<>();
        Field identifierField = null;
        Map<String, String> tableNames = new HashMap<>();
        Map<String, List<String>> fields = new HashMap<>();
        Map<String, String> fieldDescriptions = new HashMap<>();
        List<Class<?>> abstractDecoratorClasses = new ArrayList<>();
        int totalField = 0;
        int totalClasses = 0;
        String[] packageName = Arrays.copyOf(Middleware.class.getPackageName().split("\\."), 3);

        while (!clazz.getName().equals("java.lang.Object")) {
            if (clazz.getAnnotation(Table.class) != null) {
                tableNames.put(clazz.getName(), clazz.getAnnotation(Table.class).name());
            }

            if (clazz.getAnnotation(Entity.class) != null) {
                List<String> fieldNames = new ArrayList<>();
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Id.class)) {
                        identifierField = field;
                    }

                    if (Arrays.equals(packageName, Arrays.copyOf(field.getType().getPackageName().split("\\."), 3))) {
                        if (field.getAnnotation(ManyToOne.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToOne.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(OneToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(OneToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(ManyToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        }
                    } else {
                        fieldNames.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldList.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT), field.getType().getSimpleName());
                        totalField++;
                    }
                }
                classes.add(clazz);
                fields.put(clazz.getName(), fieldNames);
                totalClasses++;
            } else {
                abstractDecoratorClasses.add(clazz);
            }
            clazz = clazz.getSuperclass();
        }

        boolean firstTime = true;
        String tableNameBefore = "";
        int fieldProcessed = 0;
        int classProcessed = 0;
        for (Class<?> entityClass : classes) {
            String className = entityClass.getName();
            String tableName = tableNames.get(className);
            classProcessed++;

            for (String fieldName : fields.get(className)) {
                fieldProcessed++;
                if (fieldProcessed == totalField && classProcessed == totalClasses) {
                    // Last fields entry
                    sql.append(String.format("%s.%s \n", tableName, fieldName));

                } else {
                    sql.append(String.format("%s.%s, ", tableName, fieldName));
                }
            }

            if (firstTime) {
                tableSql.append(tableName).append("\n");
                firstTime = false;
                tableNameBefore = tableName;

                if (tableNames.entrySet().size() <= 1) {
                    tableSql.append("WHERE ")
                            .append(String.format("%s.%s", tableName, identifierField.getName()))
                            .append(" = ")
                            .append(String.format("'%s'", id));
                }
            } else {
                tableSql.append("JOIN ").append(tableName).append("\n")
                        .append(" ON ")
                        .append(String.format("%s.%s", tableName, identifierField.getName()))
                        .append(" = ")
                        .append(String.format("%s.%s", tableNameBefore, identifierField.getName()))
                        .append("\n");
                tableSql.append("AND ")
                        .append(String.format("%s.%s", tableName, identifierField.getName()))
                        .append(" = ")
                        .append(String.format("'%s'", id));
            }
        }
        sql.append(tableSql);

        RepositoryUtil<?> repositoryUtil = new RepositoryUtil<>(clazz);
        Object[] result = repositoryUtil.getObject(sql, fieldList, fieldDescriptions);
        List<Map<String, Map<String, Object>>> resultMap = new ArrayList<>();

        Map<String, Map<String, Object>> classMap = new HashMap<>();
        int idx = 0;
        for (Class<?> cls : classes) {
            Map<String, Object> fieldMap = new HashMap<>();

            for (String field : fields.get(cls.getName())) {
                fieldMap.put(field, result[idx]);
                idx++;
            }
            classMap.put(cls.getName(), fieldMap);
        }
        resultMap.add(classMap);

        return classMap;
    }

    public static void updateData(Class<?> clazz, Map<String, Object> dataMap) {
        StringBuilder sqlUpdate = new StringBuilder("");
        List<Class<?>> classes = new ArrayList<>();
        List<String> fieldList = new ArrayList<>();
        Field identifierField = null;
        Map<String, String> tableNames = new HashMap<>();
        Map<String, List<String>> fields = new HashMap<>();
        Map<String, String> fieldDescriptions = new HashMap<>();
        List<Class<?>> abstractDecoratorClasses = new ArrayList<>();
        int totalField = 0;
        int totalClasses = 0;
        String[] packageName = Arrays.copyOf(Middleware.class.getPackageName().split("\\."), 3);

        while (!clazz.getName().equals("java.lang.Object")) {
            if (clazz.getAnnotation(Table.class) != null) {
                tableNames.put(clazz.getName(), clazz.getAnnotation(Table.class).name());
            }

            if (clazz.getAnnotation(Entity.class) != null) {
                List<String> fieldNames = new ArrayList<>();
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Id.class)) {
                        identifierField = field;
                    }

                    if (Arrays.equals(packageName, Arrays.copyOf(field.getType().getPackageName().split("\\."), 3))) {
                        if (field.getAnnotation(ManyToOne.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToOne.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(OneToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(OneToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(ManyToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        }
                    } else {
                        fieldNames.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldList.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT), field.getType().getSimpleName());
                        totalField++;
                    }
                }
                classes.add(clazz);
                fields.put(clazz.getName(), fieldNames);
                totalClasses++;
            } else {
                abstractDecoratorClasses.add(clazz);
            }
            clazz = clazz.getSuperclass();
        }

        RepositoryUtil<?> repositoryUtil = new RepositoryUtil<>(clazz);
        for (int i = classes.size() - 1; i >= 0; i--) {
            Class<?> cls = classes.get(i);
            StringBuilder sql = new StringBuilder(String.format("UPDATE %s \n", tableNames.get(cls.getName())));
            sql.append("SET ");

            List<String> fieldOfEntity = fields.get(cls.getName());
            if (fieldOfEntity.size() == 0 || !fieldOfEntity.contains(identifierField.getName().toLowerCase(Locale.ROOT))) {
                fieldOfEntity.add(identifierField.getName().toLowerCase(Locale.ROOT));
                fields.put(cls.getName(), fieldOfEntity);
            }

            int idx = 1;
            for (String fieldName : fields.get(cls.getName())) {
                sql.append(fieldName).append(" = ");
                sql.append("'").append(dataMap.get(fieldName)).append("'");
                if (idx != fields.get(cls.getName()).size()) {
                    sql.append(", ");
                }
                idx++;
            }
            sql.append("\n");
            sql.append("WHERE ");
            sql.append(identifierField.getName().toLowerCase(Locale.ROOT)).append(" = ");
            sql.append("'").append(dataMap.get(identifierField.getName().toLowerCase(Locale.ROOT))).append("'");
            sql.append(";\n");
            sqlUpdate.append(sql);
        }
        repositoryUtil.executeNativeQuery(sqlUpdate);
    }

    public static void deleteData(Class<?> clazz, UUID idObject) {
        StringBuilder sqlDelete = new StringBuilder("");
        List<Class<?>> classes = new ArrayList<>();
        List<String> fieldList = new ArrayList<>();
        Field identifierField = null;
        Map<String, String> tableNames = new HashMap<>();
        Map<String, List<String>> fields = new HashMap<>();
        Map<String, String> fieldDescriptions = new HashMap<>();
        List<Class<?>> abstractDecoratorClasses = new ArrayList<>();
        int totalField = 0;
        int totalClasses = 0;
        String[] packageName = Arrays.copyOf(Middleware.class.getPackageName().split("\\."), 3);

        while (!clazz.getName().equals("java.lang.Object")) {
            if (clazz.getAnnotation(Table.class) != null) {
                tableNames.put(clazz.getName(), clazz.getAnnotation(Table.class).name());
            }

            if (clazz.getAnnotation(Entity.class) != null) {
                List<String> fieldNames = new ArrayList<>();
                for (Field field : clazz.getDeclaredFields()) {
                    if (field.isAnnotationPresent(Id.class)) {
                        identifierField = field;
                    }

                    if (Arrays.equals(packageName, Arrays.copyOf(field.getType().getPackageName().split("\\."), 3))) {
                        if (field.getAnnotation(ManyToOne.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToOne.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(OneToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(OneToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        } else if (field.getAnnotation(ManyToMany.class) != null) {
                            for (Field fieldAttribute : field.getAnnotation(ManyToMany.class).targetEntity().getDeclaredFields()) {
                                if (fieldAttribute.isAnnotationPresent(Id.class)) {
                                    fieldNames.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldList.add(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT));
                                    fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT) + "_" + fieldAttribute.getName().toLowerCase(Locale.ROOT), fieldAttribute.getType().getSimpleName());
                                    totalField++;
                                }
                            }
                        }
                    } else {
                        fieldNames.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldList.add(field.getName().toLowerCase(Locale.ROOT));
                        fieldDescriptions.put(field.getName().toLowerCase(Locale.ROOT), field.getType().getSimpleName());
                        totalField++;
                    }
                }
                classes.add(clazz);
                fields.put(clazz.getName(), fieldNames);
                totalClasses++;
            } else {
                abstractDecoratorClasses.add(clazz);
            }
            clazz = clazz.getSuperclass();
        }

        RepositoryUtil<?> repositoryUtil = new RepositoryUtil<>(clazz);
        for (int i = 0; i < classes.size(); i++) {
            Class<?> cls = classes.get(i);
            StringBuilder sql = new StringBuilder(String.format("DELETE FROM %s \n", tableNames.get(cls.getName())));
            sql.append("WHERE ").append(identifierField.getName().toLowerCase(Locale.ROOT)).append(" = ");
            sql.append("'").append(idObject).append("';").append("\n");

            sqlDelete.append(sql);
        }
//        System.out.println(sqlDelete);
        repositoryUtil.executeNativeQuery(sqlDelete);
    }

    public static Map<String, Object> objectToHashMap(Object obj) {
        Map<String, Object> hashMap = new HashMap<>();

        Class<?> clazz = obj.getClass();
        while (!clazz.getName().equals("java.lang.Object")) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                String fieldName = field.getName().toLowerCase(Locale.ROOT);
                try {
                    Object fieldValue = field.get(obj);
                    if (fieldValue != null) {
                        if (isPrimitiveOrWrapper(field.getType())) {
                            // Handle primitive types or their wrapper classes
                            hashMap.put(fieldName, fieldValue);
                        } else {
                            // Handle nested objects recursively
                            Class<?> cls = field.getType();
                            System.out.println(cls.cast(fieldValue).getClass());
                            Map<String, Object> nestedHashMap = objectToHashMap(cls.cast(fieldValue));
                            hashMap.putAll(nestedHashMap);
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            clazz = clazz.getSuperclass();
        }

        return hashMap;
    }

    private static boolean isPrimitiveOrWrapper(Class<?> clazz) {
        return clazz.isPrimitive() ||
                clazz == Boolean.class ||
                clazz == Character.class ||
                clazz == Byte.class ||
                clazz == Short.class ||
                clazz == Integer.class ||
                clazz == Long.class ||
                clazz == Float.class ||
                clazz == Double.class ||
                clazz == UUID.class ||
                clazz == String.class;
    }
}